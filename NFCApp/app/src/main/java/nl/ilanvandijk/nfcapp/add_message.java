package nl.ilanvandijk.nfcapp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class add_message extends Activity {
    private static final String TAG = Login.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_message);

        final SharedPreferences settings = getSharedPreferences("user_info", 0);

        final EditText subject = (EditText) findViewById(R.id.subjectAddMessage);
        final EditText message = (EditText) findViewById(R.id.messageAddMessage);

        Button sendMessage = (Button) findViewById(R.id.buttonAddMessage);

        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Title = "";
                String Message = "";

                if (subject.getText() != null || message.getText() != null) {
                    try {
                        Title = subject.getText().toString();
                        Message = message.getText().toString();
                    } catch (Exception ex) {
                        Log.d(TAG, ex.toString());
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Type in your username", Toast.LENGTH_SHORT).show();
                }

                String username = settings.getString("user", "User name not found");
                String password = settings.getString("pass", "password does not exist / password invalid");
                int license = settings.getInt("license", -1);

                HttpPost httppost = new HttpPost("http://www.ilanvandijk.nl/nfc/app/new_message.php");

                List<NameValuePair> namevaluepairs = new ArrayList<NameValuePair>(5);
                namevaluepairs.add(new BasicNameValuePair("u", username));
                namevaluepairs.add(new BasicNameValuePair("p", password));
                namevaluepairs.add(new BasicNameValuePair("l", Integer.toString(license)));
                namevaluepairs.add(new BasicNameValuePair("t", Title));
                namevaluepairs.add(new BasicNameValuePair("c", Message));

                try {
                    httppost.setEntity(new UrlEncodedFormEntity(namevaluepairs));

                    JsonTask task = new JsonTask();
                    task.execute(httppost);
                    String json = task.get(30, TimeUnit.SECONDS);

                    JSONObject AddMessage = new JSONObject(json);
                    String message = AddMessage.getString("message");

                    Log.d(TAG, AddMessage.toString());
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    Log.d(TAG, message);

                } catch (Exception ex) {
                    Toast.makeText(getApplicationContext(), "Exception: " + ex.toString(), Toast.LENGTH_LONG).show();
                }
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
