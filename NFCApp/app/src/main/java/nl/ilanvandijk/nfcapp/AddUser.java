package nl.ilanvandijk.nfcapp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class AddUser extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);

        final SharedPreferences settings = getSharedPreferences("user_info", 0);
        final EditText edittxtUser = (EditText) findViewById(R.id.edittxtAddUser);
        Button butInviteUser = (Button) findViewById(R.id.butAddUser);
        butInviteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HttpPost httpPost = new HttpPost("http://www.ilanvandijk.nl/nfc/app/add_user.php");
                int licenseId = settings.getInt("license", -1);
                String user = settings.getString("user", "Not found");
                String pass = settings.getString("pass", "Not found");
                String targetname = edittxtUser.getText().toString();
                try {
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                    nameValuePairs.add(new BasicNameValuePair("u", user));
                    nameValuePairs.add(new BasicNameValuePair("p", pass));
                    nameValuePairs.add(new BasicNameValuePair("l", Integer.toString(licenseId)));
                    nameValuePairs.add(new BasicNameValuePair("t", targetname));
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

                    JsonTask task = new JsonTask();
                    task.execute(httpPost);
                    String json = task.get(30, TimeUnit.SECONDS);
                    JSONObject result = new JSONObject(json);
                    String message = result.getString("message");
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                } catch (Exception ex) {
                    Log.d("AddUser", ex.toString());
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
