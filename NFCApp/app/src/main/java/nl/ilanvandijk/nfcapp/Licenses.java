package nl.ilanvandijk.nfcapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class Licenses extends Activity {
    private static final String TAG = Login.class.getSimpleName();

    List<LicenseClass> LicensesList = new ArrayList<LicenseClass>();
    ListView licensesListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_licenses);

        SharedPreferences settings = getSharedPreferences("user_info", 0);

        //Put together API URL to get licenses
        String user = settings.getString("user", "test1");
        String pass = settings.getString("pass", "fu");
        HttpPost httppost = new HttpPost("http://www.ilanvandijk.nl/nfc/app/licenses.php");
        try {
            List<NameValuePair> namevaluepairs = new ArrayList<NameValuePair>(2);
            namevaluepairs.add(new BasicNameValuePair("u", user));
            namevaluepairs.add(new BasicNameValuePair("p", pass));
            httppost.setEntity(new UrlEncodedFormEntity(namevaluepairs));

            //Calling Json API through an AsyncTask
            JsonTask task = new JsonTask();
            task.execute(httppost);
            String json = task.get(30, TimeUnit.SECONDS);
            //Use returned json string
            JSONObject result = new JSONObject(json);
            final JSONArray licenses = result.getJSONArray("licenses");
            for (int i = 0; i < licenses.length(); i++) {
                JSONObject row = licenses.getJSONObject(i);
                int id = row.getInt("id");
                String name = row.getString("name");
                String desc = row.getString("desc");
                String owner = row.getString("owner");
                addLicense(name, desc, owner, id);
                licensesListView = (ListView) findViewById(R.id.listLicenses);
                populateList();
            }

            //Set their onclicks
            licensesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        Intent intent = new Intent(Licenses.this, MessagesAndFiles.class);
                        intent.putExtra("license_id", LicensesList.get(position).getId());
                        SharedPreferences settings = getSharedPreferences("user_info", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("owner", LicensesList.get(position).getOwner());
                        editor.putInt("license", LicensesList.get(position).getId());

                        editor.commit();
                        startActivity(intent);
                    } catch (Exception ex) {
                        Log.d(TAG, "Exception: " + ex.toString());
                    }
                }
            });
        } catch (Exception ex) {
            Log.d(TAG, "Exception: " + ex.toString());
        }
    }

    private void populateList() {
        //fills the listview.
        ArrayAdapter<LicenseClass> adapter = new LicenseListAdapter();
        licensesListView.setAdapter(adapter);
    }

    private void addLicense(String name, String desc, String owner, int id) {
        //adds a license to the list
        LicensesList.add(new LicenseClass(name, desc, owner, id));
    }

    private class LicenseListAdapter extends ArrayAdapter<LicenseClass> {
        public LicenseListAdapter() {
            super(Licenses.this, R.layout.listview_item_license, LicensesList);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            //Get view method for the listview
            if (view == null)
                view = getLayoutInflater().inflate(R.layout.listview_item_license, parent, false);

            LicenseClass currentLicense = LicensesList.get(position);

            TextView name = (TextView) view.findViewById(R.id.licenseName);
            name.setText(currentLicense.getName());
            TextView desc = (TextView) view.findViewById(R.id.licenseDesc);
            desc.setText(currentLicense.getDesc());

            return view;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.licenses, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_logout_icon) {
            //Logout the user
            SharedPreferences settings = getSharedPreferences("user_info", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("loggedin", false);
            editor.putString("user", "");
            editor.putString("pass", "");
            editor.commit();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Toast backtoast;

    @Override
    public void onBackPressed() {
        if (backtoast != null && backtoast.getView().getWindowToken() != null) {
            SharedPreferences settings = getSharedPreferences("user_info", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("loggedin", false);
            editor.putString("user", "");
            editor.putString("pass", "");
            editor.commit();
            finish();
        } else {
            backtoast = Toast.makeText(this, "Press back again to log out", Toast.LENGTH_SHORT);
            backtoast.show();
        }
    }
}
