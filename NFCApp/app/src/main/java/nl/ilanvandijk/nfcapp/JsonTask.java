package nl.ilanvandijk.nfcapp;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 * Created by Matthijs on 25-9-2014.
 */
public class JsonTask extends AsyncTask<HttpPost, Void, String> {
    private static final String TAG = Login.class.getSimpleName();

    @Override
    protected String doInBackground(HttpPost... urls) {
        //HttpClient en request aanmaken met execute:
        HttpClient client = new DefaultHttpClient();
        String result = null;
        try {
            HttpResponse response = client.execute(urls[0]);
            StatusLine status = response.getStatusLine();
            if (status.getStatusCode() != 200) {
                throw new Login.ApiException("Invalid response from server: " + status.toString());
            }
            HttpEntity entity = response.getEntity();
            result = EntityUtils.toString(entity);
        } catch (Exception ex) {
            Log.println(Log.ASSERT, TAG, ex.toString());
        }
        return result;
    }
}
