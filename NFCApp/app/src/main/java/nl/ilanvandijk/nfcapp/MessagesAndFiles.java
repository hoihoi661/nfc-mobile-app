package nl.ilanvandijk.nfcapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.ipaulpro.afilechooser.utils.FileUtils;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class MessagesAndFiles extends Activity {
    private static final String TAG = Login.class.getSimpleName();

    List<Message> Messages = new ArrayList<Message>();
    ListView messageListView;

    List<FileClass> FilesList = new ArrayList<FileClass>();
    ListView fileListView;

    SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages_and_files);

        settings = getSharedPreferences("user_info", 0);

        //Change tabnames
        TabHost tabHost = (TabHost) findViewById(R.id.tabMsgAndFil);
        tabHost.setup();

        tabHost.setPadding(0, 0, 0, 0);

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("messages");
        tabSpec.setContent(R.id.tabMessages);
        tabSpec.setIndicator("Messages");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("files");
        tabSpec.setContent(R.id.tabFiles);
        tabSpec.setIndicator("Files");
        tabHost.addTab(tabSpec);
        //SharedPreferences settings = getSharedPreferences("user_info", 0);

        fillLists();
    }

    public void fillLists() {
        Messages.clear();
        FilesList.clear();
        final int id = settings.getInt("license", -1);
        final String user = settings.getString("user", "test1");
        final String pass = settings.getString("pass", "fu");
        HttpPost httppost = new HttpPost("http://www.ilanvandijk.nl/nfc/app/messages.php");

        Messages.clear();
        FilesList.clear();
        try {
            List<NameValuePair> namevaluepairs = new ArrayList<NameValuePair>(3);
            namevaluepairs.add(new BasicNameValuePair("u", user));
            namevaluepairs.add(new BasicNameValuePair("p", pass));
            namevaluepairs.add(new BasicNameValuePair("l", Integer.toString(id)));
            httppost.setEntity(new UrlEncodedFormEntity(namevaluepairs));

            //Calling Json API through an AsyncTask
            JsonTask task = new JsonTask();
            task.execute(httppost);
            String json = task.get(30, TimeUnit.SECONDS);
            //Use returned json string
            JSONObject result = new JSONObject(json);
            final JSONArray messages = result.getJSONArray("messages");
            for (int i = 0; i < messages.length(); i++) {
                JSONObject row = messages.getJSONObject(i);
                String title = row.getString("title");
                String message = row.getString("message");
                String creator = row.getString("creator");
                String created = row.getString("created");
                String messageid = row.getString("id");

                addMessage(title, message, creator, created, messageid);
                messageListView = (ListView) findViewById(R.id.listMessages);
                populateList();
            }

            messageListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    try {
                        Intent intent = new Intent(MessagesAndFiles.this, FullMessage.class);
                        Message currentMessage = Messages.get(position);
                        intent.putExtra("message_title", currentMessage.getTitle());
                        intent.putExtra("message_text", currentMessage.getMessage());
                        intent.putExtra("message_id", currentMessage.getId());

                        Date time = new Date(Long.parseLong(currentMessage.getCreated()) * 1000);
                        String created = new SimpleDateFormat("EEEE d MMMM yyyy 'at' kk:mm zzz").format(time);
                        intent.putExtra("message_created", "Created by " + currentMessage.getCreator() + " on " + created);
                        startActivity(intent);
                    } catch (Exception ex) {
                        Log.d(TAG, "Exception: " + ex);
                    }
                }
            });
        } catch (Exception ex) {
            Log.d(TAG, "Exception: " + ex);
        }

        httppost = new HttpPost("http://www.ilanvandijk.nl/nfc/app/files.php");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
            nameValuePairs.add(new BasicNameValuePair("u", user));
            nameValuePairs.add(new BasicNameValuePair("p", pass));
            nameValuePairs.add(new BasicNameValuePair("l", Integer.toString(id)));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

            //Calling Json API through an AsyncTask
            JsonTask task = new JsonTask();
            task.execute(httppost);
            String json = task.get(30, TimeUnit.SECONDS);
            //Use returned json string
            JSONObject result = new JSONObject(json);
            final JSONArray files = result.getJSONArray("files");
            for (int x = 0; x < files.length(); x++) {
                JSONObject row = files.getJSONObject(x);
                String title = row.getString("title");
                String url = row.getString("url");
                String creator = row.getString("creator");
                String created = row.getString("created");
                int fileId = row.getInt("id");
                Date date = new Date(Long.parseLong(created) * 1000);
                created = new SimpleDateFormat("EEEE d MMMM yyyy 'at' kk:mm zzz").format(date);

                addFile(title, url, creator, created, fileId);
            }
            fileListView = (ListView) findViewById(R.id.listFiles);
            populateFileList();

            fileListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        FileClass currentFile = FilesList.get(i);
                        //String fileId = Integer.toString(currentFile.get_fileId());
                        String url = currentFile.get_url();
                        String fileName = url.substring(url.lastIndexOf("/") + 1);
                        String path = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getPath();
                        File file = new File(path + "/" + fileName);
                        if (file.exists()) {
                            FileOpen.openFile(getApplicationContext(), file, url);
                        } else {
                            /*HttpPost httppost = new HttpPost("http://www.ilanvandijk.nl/nfc/app/download.php");
                            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                            nameValuePairs.add(new BasicNameValuePair("u", user));
                            nameValuePairs.add(new BasicNameValuePair("p", pass));
                            nameValuePairs.add(new BasicNameValuePair("l", Integer.toString(id)));
                            nameValuePairs.add(new BasicNameValuePair("f", fileId));
                            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                            JsonTask task = new JsonTask();
                            task.execute(httppost);
                            String json = task.get(60, TimeUnit.SECONDS);
                            JSONObject result = new JSONObject(json);
                            String message = result.getString("message");*/
                            DownloadTask downloader = new DownloadTask();
                            downloader.execute("http://www.ilanvandijk.nl/nfc/" + url, fileName, path);
                            String message = downloader.get(60, TimeUnit.SECONDS);
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception ex) {
                        Log.d(TAG, "Exception: " + ex);
                    }
                }
            });

        } catch (Exception ex) {
            Log.d(TAG, "Exception: " + ex);
        }
    }

    public void clickHandlerFilesDelete(View v) {
        final String fileId = v.getTag().toString();
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        HttpPost httpPost = new HttpPost("http://www.ilanvandijk.nl/nfc/app/delete_file.php");
                        int licenseId = settings.getInt("license", -1);
                        String user = settings.getString("user", "test1");
                        String pass = settings.getString("pass", "fu");
                        FileClass currentFile = FilesList.get(0);
                        for(FileClass file : FilesList){
                            if(Integer.toString(file.get_fileId()) == fileId)
                                currentFile = file;
                        }
                        String url = currentFile.get_url();
                        url = url.replace(" ", "%20");
                        String fileName = url.substring(url.lastIndexOf("/") + 1);
                        try {
                            String path = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getPath();
                            File file = new File(path + "/" + fileName);
                            boolean deleted = false;
                            if(file.exists()){
                                deleted = file.delete();
                            }
                            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                            nameValuePairs.add(new BasicNameValuePair("u", user));
                            nameValuePairs.add(new BasicNameValuePair("p", pass));
                            nameValuePairs.add(new BasicNameValuePair("l", Integer.toString(licenseId)));
                            nameValuePairs.add(new BasicNameValuePair("f", fileId));
                            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

                            JsonTask task = new JsonTask();
                            task.execute(httpPost);
                            String json = task.get(30, TimeUnit.SECONDS);
                            JSONObject result = new JSONObject(json);
                            String message = result.getString("message");
                            if(deleted)
                                message += " Also deleted local file.";
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            fillLists();
                        } catch (Exception ex) {
                            Log.d(TAG, ex.toString());
                        }
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you really want to delete this file?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    public void clickHandlerFilesDownload(View v) {
        final String fileId = v.getTag().toString();
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //HttpPost httpPost = new HttpPost("http://www.ilanvandijk.nl/nfc/app/download.php");
                        //int licenseId = settings.getInt("license", -1);
                        //String user = settings.getString("user", "test1");
                        //String pass = settings.getString("pass", "fu");
                        FileClass currentFile = FilesList.get(0);
                        for (FileClass file : FilesList) {
                            if (Integer.toString(file.get_fileId()).equals(fileId)) {
                                currentFile = file;
                            }
                        }
                        String url = currentFile.get_url();
                        url = url.replace(" ", "%20");
                        String fileName = url.substring(url.lastIndexOf("/") + 1);
                        try {
                            String path = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getPath();
                            File file = new File(path + "/" + fileName);
                            /*List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
                            nameValuePairs.add(new BasicNameValuePair("u", user));
                            nameValuePairs.add(new BasicNameValuePair("p", pass));
                            nameValuePairs.add(new BasicNameValuePair("l", Integer.toString(licenseId)));
                            nameValuePairs.add(new BasicNameValuePair("f", fileId));
                            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

                            JsonTask task = new JsonTask();
                            task.execute(httpPost);
                            String json = task.get(60, TimeUnit.SECONDS);
                            JSONObject result = new JSONObject(json);
                            String message = result.getString("message");*/
                            if (file.exists()) {
                                Toast.makeText(getApplicationContext(), "Already have a file with the same name!", Toast.LENGTH_LONG).show();
                                FileOpen.openFile(getApplicationContext(), file, url);
                            } else {
                                DownloadTask downloader = new DownloadTask();
                                downloader.execute("http://www.ilanvandijk.nl/nfc/" + url, fileName, path);
                                String message = downloader.get(60, TimeUnit.SECONDS);
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            Log.d(TAG, ex.toString());
                        }
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you really want to download this file?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private void populateList() {
        //fills the listview
        ArrayAdapter<Message> adapter = new MessageListAdapter();
        messageListView.setAdapter(adapter);
    }

    private void addMessage(String title, String message, String creator, String created, String messageid) {
        //adds a message to the list
        Messages.add(new Message(title, message, creator, created, messageid));
    }

    private class MessageListAdapter extends ArrayAdapter<Message> {
        public MessageListAdapter() {
            super(MessagesAndFiles.this, R.layout.listview_item_message, Messages);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null)
                view = getLayoutInflater().inflate(R.layout.listview_item_message, parent, false);

            Message currentMessage = Messages.get(position);

            TextView title = (TextView) view.findViewById(R.id.messageTitle);
            title.setText(currentMessage.getTitle());
            TextView text = (TextView) view.findViewById(R.id.messageText);
            text.setText(currentMessage.getMessage());

            return view;
        }
    }

    private void populateFileList() {
        //fills the fileListView
        ArrayAdapter<FileClass> adapter = new FileListAdapter();
        fileListView.setAdapter(adapter);
    }

    private void addFile(String title, String url, String creator, String created, int fileId) {
        FilesList.add(new FileClass(title, url, creator, created, fileId));
    }

    private class FileListAdapter extends ArrayAdapter<FileClass> {
        public FileListAdapter() {
            super(MessagesAndFiles.this, R.layout.listview_item_file, FilesList);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null)
                view = getLayoutInflater().inflate(R.layout.listview_item_file, parent, false);

            FileClass currentFile = FilesList.get(position);

            TextView title = (TextView) view.findViewById(R.id.fileTitle);
            title.setText(currentFile.get_title());
            TextView creator = (TextView) view.findViewById(R.id.fileCreator);
            creator.setText(currentFile.get_creator());
            TextView created = (TextView) view.findViewById(R.id.fileCreated);
            created.setText(currentFile.get_created());
            Button delete = (Button) view.findViewById(R.id.butDelFile);
            delete.setTag(currentFile.get_fileId());
            Button download = (Button) view.findViewById(R.id.butDownloadFile);
            download.setTag(currentFile.get_fileId());

            return view;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.messages_and_files, menu);
        String userId = settings.getString("userId", "User not found");
        String ownerId = settings.getString("owner", "Owner not found");
        MenuItem addMessageItem = menu.findItem(R.id.action_addMessage);
        MenuItem addFileItem = menu.findItem(R.id.action_addFile);
        if (!userId.equals(ownerId)) {
            addMessageItem.setVisible(false);
            addFileItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_addMessage) {
            Intent intent = new Intent(MessagesAndFiles.this, add_message.class);
            boolean tomessage = true;
            if (tomessage) {
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "Something is going wrong! try again", Toast.LENGTH_LONG).show();
            }

            return true;

        } else if (id == R.id.action_addFile) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            Intent getContentIntent = FileUtils.createGetContentIntent();

                            Intent intent = Intent.createChooser(getContentIntent, "Select a file");
                            startActivityForResult(intent, 1);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Do you really want to upload a file? (ASTRO file manager is recommended.)").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        } else if (id == R.id.action_refresh) {
            fillLists();
        } else if (id == R.id.action_inviteUser) {
            Intent intent = new Intent(this, AddUser.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    final Uri uri = data.getData();

                    // Get the File path from the Uri
                    String path = FileUtils.getPath(this, uri);

                    // Alternatively, use FileUtils.getFile(Context, Uri)
                    if (path != null && FileUtils.isLocal(path)) {
                        File file = new File(path);

                        SharedPreferences settings = getSharedPreferences("user_info", 0);
                        HttpPost httppost = new HttpPost("http://www.ilanvandijk.nl/nfc/app/upload.php");
                        try {
                            MultipartEntity entity = new MultipartEntity();
                            entity.addPart("u", new StringBody(settings.getString("user", "Not found")));
                            entity.addPart("p", new StringBody(settings.getString("pass", "Not found")));
                            entity.addPart("l", new StringBody(Integer.toString(settings.getInt("license", 0))));
                            entity.addPart("f", new FileBody(file));
                            httppost.setEntity(entity);
                            JsonTask task = new JsonTask();
                            task.execute(httppost);

                            String json = task.get(60, TimeUnit.SECONDS);
                            JSONObject result = new JSONObject(json);
                            String message = result.getString("message");

                            Log.d(TAG, result.toString());
                            if (message != null)
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            fillLists();
                        } catch (Exception ex) {
                            Log.d(TAG, "Exception: " + ex.toString());
                        }
                    }
                } else {
                    Log.d(TAG, "Didn't reach Result.OK");
                }
                break;
            default:
                Toast.makeText(getApplicationContext(), "Something went wrong whilst choosing a file.", Toast.LENGTH_SHORT).show();
                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        fillLists();
    }
}
