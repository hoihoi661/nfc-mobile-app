package nl.ilanvandijk.nfcapp;

import android.os.AsyncTask;
/**
 * Created by Matthijs on 9-10-2014.
 */
public class DownloadTask extends AsyncTask<String, String, String> {

    @Override
    protected String doInBackground(String... info) {
        String message = "";
        try {
            DownloadManager.DownloadFromUrl(info[0], info[1], info[2]);
        } catch (Exception ex) {
            message = "Exception: " + ex;
        }
        message = "File was downloaded successfully!";
        return message;
    }
}
