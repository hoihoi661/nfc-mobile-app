package nl.ilanvandijk.nfcapp;

/**
 * Created by Matthijs on 30-9-2014.
 */
public class LicenseClass {

    private String _name, _desc, _owner;
    private int _id;

    public LicenseClass(String name, String desc, String owner, int id) {
        _name = name;
        _desc = desc;
        _owner = owner;
        _id = id;
    }

    public String getName() {
        return _name;
    }

    public String getDesc() {
        return _desc;
    }

    public int getId() {
        return _id;
    }

    public String getOwner() {
        return _owner;
    }
}
