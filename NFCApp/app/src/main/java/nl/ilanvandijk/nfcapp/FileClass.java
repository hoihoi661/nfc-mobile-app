package nl.ilanvandijk.nfcapp;

/**
 * Created by lammert on 2-10-2014.
 */
public class FileClass {

    private String _title, _url, _creator, _created;
    private int _fileId;

    public FileClass(String title, String url, String creator, String created, int fileId) {
        _title = title;
        _url = url;
        _creator = creator;
        _created = created;
        _fileId = fileId;
    }

    public String get_title() {
        return _title;
    }

    public String get_url() {
        return _url;
    }

    public String get_creator() {
        return _creator;
    }

    public String get_created() {
        return _created;
    }

    public int get_fileId() {
        return _fileId;
    }
}
