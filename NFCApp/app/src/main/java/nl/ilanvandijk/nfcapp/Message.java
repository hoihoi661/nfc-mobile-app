package nl.ilanvandijk.nfcapp;

/**
 * Created by Matthijs on 1-10-2014.
 */
public class Message {

    private String _title, _message, _creator, _created, _messageid;

    public Message(String title, String message, String creator, String created, String messageid) {
        _title = title;
        _message = message;
        _creator = creator;
        _created = created;
        _messageid = messageid;
    }

    public String getTitle() {
        return _title;
    }

    public String getMessage() {
        return _message;
    }

    public String getCreator() {
        return _creator;
    }

    public String getCreated() {
        return _created;
    }

    public String getId() {
        return _messageid;
    }
}
