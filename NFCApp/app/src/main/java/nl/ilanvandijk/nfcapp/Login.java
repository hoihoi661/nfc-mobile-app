package nl.ilanvandijk.nfcapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class Login extends Activity {
    //private static final String TAG = Login.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Check if already loggedin
        SharedPreferences settings = getSharedPreferences("user_info", 0);
        boolean loggedin = settings.getBoolean("loggedin", false);
        if (loggedin) {
            //Open licenses activity
            Intent licences = new Intent(getApplicationContext(), Licenses.class);
            startActivity(licences);
        }
        setContentView(R.layout.activity_login);

        final EditText username = (EditText) findViewById(R.id.edittxtUsername);
        final EditText password = (EditText) findViewById(R.id.edittxtPassword);
        Button signinButton = (Button) findViewById(R.id.butSignin);

        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Put together API URL
                String user = username.getText().toString();
                String pass = password.getText().toString();
                HttpPost httppost = new HttpPost("http://www.ilanvandijk.nl/nfc/app/login.php");
                try {
                    List<NameValuePair> namevaluepairs = new ArrayList<NameValuePair>(2);
                    namevaluepairs.add(new BasicNameValuePair("u", user));
                    namevaluepairs.add(new BasicNameValuePair("p", pass));
                    httppost.setEntity(new UrlEncodedFormEntity(namevaluepairs));

                    //Calling Json API through an AsyncTask
                    JsonTask task = new JsonTask();
                    task.execute(httppost);
                    String json = task.get(30, TimeUnit.SECONDS);
                    //Use returned JSON
                    JSONObject ApiResult = new JSONObject(json);
                    String message = ApiResult.getString("message");

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    if (message.equals("Succesfully loggedin.")) {
                        JSONObject account = ApiResult.getJSONObject("account");
                        String userId = account.getString("user_id");

                        //Safe login info
                        SharedPreferences settings = getSharedPreferences("user_info", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putBoolean("loggedin", true);
                        editor.putString("user", user);
                        editor.putString("pass", pass);
                        editor.putString("userId", userId);

                        editor.commit();

                        //Open licenses activity
                        Intent licences = new Intent(v.getContext(), Licenses.class);
                        startActivity(licences);
                    }
                } catch (Exception ex) {
                    Toast.makeText(getApplicationContext(), "Exception: " + ex.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });

        //Link to create new account on site
        TextView linkToWebsite = (TextView) findViewById(R.id.linkToWebsite);
        linkToWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://ilanvandijk.nl/nfc/website/index.php#signin-page"));
                startActivity(browserIntent);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class ApiException extends Exception {
        private static final long serialVersionUID = 1L;

        public ApiException(String msg) {
            super(msg);
        }
    }
}
