package nl.ilanvandijk.nfcapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class FullMessage extends Activity {
    private static final String TAG = Login.class.getSimpleName();

    SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_message);

        settings = getSharedPreferences("user_info", 0);

        try {
            Intent intent = getIntent();
            String title = intent.getStringExtra("message_title");
            String text = intent.getStringExtra("message_text");
            String created = intent.getStringExtra("message_created");
            TextView messageTitle = (TextView) findViewById(R.id.fullMessageTitle);
            TextView messageText = (TextView) findViewById(R.id.fullMessageText);
            TextView messageCreated = (TextView) findViewById(R.id.fullMessageCreated);

            messageTitle.setText(title);
            messageText.setText(text);
            messageCreated.setText(created);
        } catch (Exception ex) {
            Log.d(TAG, "Exception: " + ex.toString());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.full_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.delete_message) {
            String user;
            String pass;
            String messageid;
            int licenseId = 0;

            user = settings.getString("user", "user name not found");
            pass = settings.getString("pass", "password is not found");
            licenseId = settings.getInt("license", -1);
            messageid = getIntent().getStringExtra("message_id");

            HttpPost httppost = new HttpPost("http://www.ilanvandijk.nl/nfc/app/delete_message.php");

            List<NameValuePair> namevaluepairs = new ArrayList<NameValuePair>(5);
            namevaluepairs.add(new BasicNameValuePair("u", user));
            namevaluepairs.add(new BasicNameValuePair("p", pass));
            namevaluepairs.add(new BasicNameValuePair("l", Integer.toString(licenseId)));
            namevaluepairs.add(new BasicNameValuePair("m", messageid));

            try {
                httppost.setEntity(new UrlEncodedFormEntity(namevaluepairs));

                JsonTask task = new JsonTask();
                task.execute(httppost);
                String json = task.get(30, TimeUnit.SECONDS);

                JSONObject AddMessage = new JSONObject(json);
                String message = AddMessage.getString("message");

                Log.d(TAG, AddMessage.toString());
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                Log.d(TAG, message);
            } catch (Exception ex) {
                Toast.makeText(getApplicationContext(), "Exception: " + ex.toString(), Toast.LENGTH_LONG).show();
            }

            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
